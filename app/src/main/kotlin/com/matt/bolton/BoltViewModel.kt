
package com.matt.bolton

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.util.Deque
import java.util.LinkedList

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.net.Uri
import android.provider.OpenableColumns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext

import com.matt.fitgpxconverter.FITGPXOutputStream

import com.matt.bolton.bolt.BCmdDeleteFile
import com.matt.bolton.bolt.BCmdGetFile
import com.matt.bolton.bolt.BCmdListFiles
import com.matt.bolton.bolt.BCmdLoadPlan
import com.matt.bolton.bolt.BCmdSendFile
import com.matt.bolton.bolt.BCmdStartRoute
import com.matt.bolton.bolt.BOLT_SEP
import com.matt.bolton.bolt.BoltBT
import com.matt.bolton.bolt.FileListingType
import com.matt.bolton.bolt.PLAN_FILE_EXT
import com.matt.bolton.bolt.PLAYABLE_ROUTES_DIR_CODE
import com.matt.bolton.bolt.PLAYABLE_PLANS_DIR_CODE

public class BoltViewModel(application : Application)
        : AndroidViewModel(application) {

    private val FIT_FILE_EXTENSION = ".fit"
    private val GPX_FILE_EXTENSION = ".gpx"

    public enum class FileActionStatus {
        NONE, SENDING, GETTING, SUCCEEDED, FAILED
    }

    public enum class ConnectionStatus {
        DISCONNECTED, CONNECTING, CONNECTED
    }

    public class FileActionInfo(var status : FileActionStatus,
                                var filename : String?,
                                var progress : Int)

    public class BoltFileEntry(val filename : String,
                               val baseDirectory : String,
                               val isDirectory : Boolean,
                               val size : Int,
                               val timestamp : Int) {
        val fullPath by lazy { baseDirectory + filename }

        val routeCode : Short
            = PLAYABLE_ROUTES_DIR_CODE[baseDirectory] ?: 0.toShort()

        val planCode : Byte
            = PLAYABLE_PLANS_DIR_CODE[baseDirectory] ?: 0.toByte()

        val isPlayableRoute : Boolean
            = (!isDirectory && baseDirectory in PLAYABLE_ROUTES_DIR_CODE)

        val isPlayablePlan : Boolean
            = (!isDirectory &&
               baseDirectory in PLAYABLE_PLANS_DIR_CODE &&
               filename.endsWith(PLAN_FILE_EXT))

        val isPlayable : Boolean
            = isPlayableRoute || isPlayablePlan
    }

    /**
     * Listing of a directory
     */
    public class BoltListing(val path : String,
                             val hasParent : Boolean,
                             val files : List<BoltFileEntry>)

    private class ListingInfo(val pathStack : PathStack,
                              val listing : MutableLiveData<BoltListing>)

    var device : BluetoothDevice? = null
        private set
    // this is a bit of a hack, but save it here so that FileListingFragment
    // can save it for onActivityResult of file picker (can't save it in
    // fragment in case it's destroyed)
    var inFileForGetting : BoltFileEntry? = null

    val connectionStatus = MutableLiveData<ConnectionStatus>()
    val fileActionInfo = MutableLiveData<FileActionInfo>()
    private val listings = mapOf(
        *FileListingType.values()
                        .map({
            it to ListingInfo(PathStack(it.baseDirectory),
                              MutableLiveData<BoltListing>())
        }).toTypedArray()
    )

    // only one file op at a time -- a UI constraint, but also possibly a Bolt
    // one!
    val fileActionMutex = Mutex()
    // else listings get confused
    val fileListMutex = Mutex()

    // somehow it's easier than calling getApplication through
    // coroutines
    val app : Application = getApplication()

    private var boltbt : BoltBT? = null

    init {
        connectionStatus.postValue(ConnectionStatus.DISCONNECTED)
        fileActionInfo.postValue(FileActionInfo(FileActionStatus.NONE, null, 0))
        for ((_, listing) in listings) {
            listing.listing.postValue(
                BoltListing(listing.pathStack.baselessPath,
                            listing.pathStack.hasParent,
                            listOf())
            )
        }
    }

    /**
     * Create a BoltBT connection to the given device
     */
    fun connect(device : BluetoothDevice) {
        if (boltbt != null) {
            return
        }

        boltbt = BoltBT()
        viewModelScope.launch(Dispatchers.IO) {
            val onDisconnect = {
                boltbt = null
                connectionStatus.postValue(ConnectionStatus.DISCONNECTED)
            }
            connectionStatus.postValue(ConnectionStatus.CONNECTING)
            val res = boltbt?.connect(device, app, onDisconnect)
            if (res == true) {
                this@BoltViewModel.device = device
                connectionStatus.postValue(ConnectionStatus.CONNECTED)
            } else {
                this@BoltViewModel.device = null
                boltbt = null
                connectionStatus.postValue(ConnectionStatus.DISCONNECTED)
            }
        }
    }

    fun disconnect() {
        boltbt?.close()
        boltbt = null
    }

    override fun onCleared() {
        boltbt?.close()
        boltbt = null
    }

    /**
     * Get the mutable live data listing of the given file type
     */
    fun getFileListingLiveData(listType : FileListingType)
        : MutableLiveData<BoltListing> {
        return listings[listType]!!.listing
    }

    /**
     * Get currently selected directory of listType
     */
    fun getCurrentPath(listType : FileListingType) : String {
        return listings[listType]!!.pathStack.toString()
    }

    /**
     * If currently doing a file op
     */
    val isFileActionActive : Boolean
        get() {
            val fai = fileActionInfo.value
            if (fai == null)
                return false

            return (fai.status == FileActionStatus.SENDING ||
                    fai.status == FileActionStatus.GETTING)
        }

    /**
     * Ask the Bolt for an updated listing of the given type of files
     *
     * Avoid repeated calls to this method: Bolt responds to file
     * listings differently each time!
     *
     * TODO: implement some guard on this -- i.e. don't ask twice about
     * same directory in a row unless there was a file action
     */
    fun refreshFileListing(listType : FileListingType) {
        val nnBolt = boltbt
        if (nnBolt == null)
            return

        viewModelScope.launch {
            fileListMutex.withLock {
                val pathStack = listings[listType]!!.pathStack
                val fullPath = pathStack.toString()
                val listing = listings[listType]!!.listing

                var fileRes = BCmdListFiles(fullPath).send(nnBolt)
                val files = fileRes.files.map({
                    BoltFileEntry(it.filename,
                                  fullPath,
                                  it.isDirectory,
                                  it.size,
                                  it.timestamp)
                })
                val path = pathStack.baselessPath
                val hasParent = pathStack.hasParent

                listing.postValue(BoltListing(path, hasParent, files))
            }
        }
    }

    /**
     * Navigate to a subdirectory for the given listing
     *
     * Does not check if the directory exists, you will get empty
     * results in this case
     */
    fun pushDirectory(fileListingType : FileListingType,
                      dirname : String) {
        viewModelScope.launch(Dispatchers.IO) {
            listings[fileListingType]!!.pathStack.pushDir(dirname)
            refreshFileListing(fileListingType)
        }
    }

    /**
     * Move a level up in file listing directory structure
     *
     * Stays at base if already at base, returns false in this case
     */
    fun popDirectory(fileListingType : FileListingType) : Boolean {
        val stack = listings[fileListingType]!!.pathStack
        if (!stack.hasParent) {
            return false
        }

        viewModelScope.launch(Dispatchers.IO) {
            stack.popDir()
            refreshFileListing(fileListingType)
        }

        return true
    }

    /**
     * Starts a send file operation
     *
     * Returns immediately if not connected.
     */
    fun sendFile(fileUri : Uri, fileListingType : FileListingType) {
        val nnBolt = boltbt
        if (nnBolt == null)
            return

        val info = getFileInfo(fileUri)
        if (info == null)
            return

        val basename = info.filename
        val size = info.size


        viewModelScope.launch(Dispatchers.IO) {
            fileActionMutex.withLock {
                setFileActionInfo(FileActionStatus.SENDING, basename, 0)

                val inFile = BufferedInputStream(
                    app.getContentResolver()
                       .openInputStream(fileUri)
                )

                val dirPath
                    = listings[fileListingType]!!.pathStack.toString()
                val dest = dirPath + basename
                val progressCB = { bytes : Int ->
                    val progress
                        = (100 * bytes / size.toFloat()).toInt()
                    setFileActionInfo(FileActionStatus.SENDING,
                                      basename,
                                      progress)
                }
                val res = BCmdSendFile(inFile,
                                       dest,
                                       progressCB).send(nnBolt)

                // this feels weird that it's not try/finally, but
                // kotlin tells me it's not needed...
                inFile.close()

                if (res.success) {
                    setFileActionInfo(FileActionStatus.SUCCEEDED,
                                      basename,
                                      100)
                    refreshFileListing(fileListingType)
                } else {
                    setFileActionInfo(FileActionStatus.FAILED, basename, 0)
                }
            }
        }
    }

    /**
     * Starts a get file operation
     *
     * Returns immediately if not connected. inFileName is file name on Bolt,
     * fileUri is where to save it on Android.
     *
     * If fileEntry ends ".fit" and the fileUri ends ".gpx" then the FIT
     * is translated to GPX.
     */
    fun getFile(fileEntry : BoltFileEntry, fileUri : Uri) {
        val nnBolt = boltbt
        if (nnBolt == null)
            return

        viewModelScope.launch(Dispatchers.IO) {
            fileActionMutex.withLock {
                val inFilepath = fileEntry.fullPath
                val inFilename = fileEntry.filename

                setFileActionInfo(FileActionStatus.GETTING, inFilename, 0)

                val contentOs = app.getContentResolver()
                                   .openOutputStream(fileUri)
                val outputStream =
                    if (isConvertFITGPX(inFilename, fileUri.getPath()))
                        FITGPXOutputStream(contentOs)
                    else
                        BufferedOutputStream(contentOs)

                val progressCB = { bytes : Int, size : Int ->
                    val progress
                        = (100 * bytes / size.toFloat()).toInt()
                    setFileActionInfo(FileActionStatus.GETTING,
                                      inFilename,
                                      progress)
                }

                val res = BCmdGetFile(inFilepath,
                                      outputStream,
                                      progressCB).send(nnBolt)

                // this feels weird that it's not try/finally, but
                // kotlin tells me it's not needed...
                outputStream.close()

                if (res.success) {
                    setFileActionInfo(FileActionStatus.SUCCEEDED,
                                      inFilename,
                                      100)
                } else {
                    setFileActionInfo(FileActionStatus.FAILED, inFilename, 0)
                }
            }
        }
    }

    /**
     * Request the file is deleted
     */
    fun deleteFile(listType : FileListingType, fileEntry : BoltFileEntry) {
        val nnBolt = boltbt
        if (nnBolt == null)
            return

        viewModelScope.launch {
            val res = BCmdDeleteFile(fileEntry.fullPath).send(nnBolt)
            if (res.success)
                refreshFileListing(listType)
        }
    }

    /**
     * Request to start the file if playable (e.g. route)
     */
    fun playFile(fileEntry : BoltFileEntry) {
        val nnBolt = boltbt
        if (nnBolt == null)
            return

        if (!fileEntry.isPlayable)
            return

        viewModelScope.launch {
            if (fileEntry.isPlayableRoute) {
                BCmdStartRoute(fileEntry.routeCode,
                               fileEntry.filename).send(nnBolt)
            } else {
                BCmdLoadPlan(fileEntry.planCode,
                             fileEntry.filename).send(nnBolt)
            }
        }
    }

    /**
     * Returns name and size of file
     */
    private fun getFileInfo(uri : Uri) : FileInfo? {
        val returnCursor
            = app.getContentResolver()
                 .query(uri, null, null, null, null)

        if (returnCursor == null)
            return null

        val nameIndex
            = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        val sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE)

        returnCursor.moveToFirst()

        val name = returnCursor.getString(nameIndex)
        val size = returnCursor.getInt(sizeIndex)

        returnCursor.close();

        return FileInfo(name, size)
    }

    private fun <T> notifyUpdate(field : MutableLiveData<T>) {
        field.postValue(field.value)
    }

    private fun setFileActionInfo(status : FileActionStatus,
                                  filename : String?,
                                  progress : Int) {
        fileActionInfo.value?.status = status
        fileActionInfo.value?.filename = filename
        fileActionInfo.value?.progress = progress
        notifyUpdate(fileActionInfo)
    }

    private class FileInfo(val filename : String, val size : Int)

    private class PathStack(val baseDirectory : String) {
        val stack : Deque<String> = LinkedList<String>()
        val baselessPath : String
            get() {
                if (stack.isEmpty())
                    return ""
                else
                    return stack.joinToString(BOLT_SEP) + BOLT_SEP
            }
        val hasParent : Boolean
            get() = !stack.isEmpty()

        fun pushDir(dirname : String) {
            stack.add(dirname)
        }

        fun popDir() : Boolean {
            if (stack.isEmpty()) {
                return false
            } else {
                stack.removeLast()
                return true
            }
        }

        override fun toString() : String {
            return baseDirectory + baselessPath
        }
    }

    /**
     * For the convenience of FileListingFragment
     */
    public fun isFITFile(filename : String?) : Boolean {
        if (filename == null)
            return false
        return endsWithIgnoreCase(filename, FIT_FILE_EXTENSION)
    }

    /**
     * For the convenience of FileListingFragment
     *
     * Assumes isFITFile is true
     */
    public fun changeFITExtensionToGPX(filename : String) : String {
        return filename.removeSuffix(FIT_FILE_EXTENSION) + GPX_FILE_EXTENSION
    }


    /**
     * Indicates whether a FIT-GPX conversion should take place
     */
    private fun isConvertFITGPX(inFilename : String?,
                                outFilename : String?) : Boolean {
        return (isFITFile(inFilename) &&
                endsWithIgnoreCase(outFilename, GPX_FILE_EXTENSION))
    }

    /**
     * Taken from
     * https://stackoverflow.com/a/38947571
     */
    private fun endsWithIgnoreCase(str : String?, suffix : String?) : Boolean {
        if (str == null || suffix == null)
            return false
        val suffixLength = suffix.length;
        return str.regionMatches(str.length - suffixLength,
                                 suffix,
                                 0,
                                 suffixLength,
                                 true);
    }
}
