
package com.matt.bolton.bolt

import java.io.IOException
import java.io.OutputStream
import java.nio.ByteBuffer
import java.util.zip.ZipException
import kotlinx.coroutines.channels.ClosedReceiveChannelException

/**
 * Read a local file from the Bolt
 *
 * inFilename should be fully qualified on the device. outFile stream should be
 * created and closed by the user.
 *
 * progressCallback takes the number of bytes received so far and the
 * file size
 */
public class BCmdGetFile(
    val inFilename : String,
    val outFile : OutputStream,
    val progressCallback : (Int, Int) -> Unit = { _, _ -> }) : BoltCommand() {

    val collector = BoltBytesCollector(CMD_FILE_BODY, CMD_FILE_END)

    public class BCmdResGetFile(success : Boolean,
                                val inFilename : String)
            : BoltCommand.BoltCommandResult(success) { }


    override suspend fun send(bolt : BoltBT) : BCmdResGetFile {
        val success = doSend(bolt)
        return BCmdResGetFile(success, inFilename)
    }

    /**
     * Does file getting, returns true on success
     */
    private suspend fun doSend(bolt : BoltBT) : Boolean {
        val msgID = bolt.getNextMessageID()
        val buffer = getBoltByteBuffer(inFilename.length + 7)
        buffer.putIntBytes(0, FILE_ACTION_GET)
        buffer.putBoltString(inFilename)
        buffer.putIntBytes(0, 0, 0, 0, 0)

        var listener : BoltBytesListener? = null
        try {
            listener = getFileListener(bolt, msgID)
            listener.register()

            sendLongMessage(bolt,
                            FILE_UUID,
                            CMD_FILE_NAME_BODY,
                            CMD_FILE_NAME_END,
                            msgID,
                            buffer.array())

            val infoMsg = listener.receive()

            if (infoMsg[0] != CMD_FILE_INFO.toByte()) {
                return false
            }

            val fileSize = getBoltByteBuffer(infoMsg, 7, 4).getInt()
            if (fileSize == 0)
                return false

            var bytesReceived = 0

            while (bytesReceived < fileSize) {
                val fullMsg = collector.getFullMessage(listener)

                val chunk = gunzipBytes(fullMsg, 7, fullMsg.size - 7)
                outFile.write(chunk)
                bytesReceived += chunk.size

                System.out.println("BOLTBT: received " + bytesReceived)

                notifyProgress(bytesReceived, fileSize)
            }

            val doneMsg = listener.receive()
            if (doneMsg[0] != CMD_FILE_DONE.toByte())
                return false

            return true
        } catch (e : ClosedReceiveChannelException) {
            /* do nothing */
        } catch (e : IOException) {
            /* do nothing */
        } catch (e : ZipException) {
            /* do nothing */
        } finally {
            listener?.close()
        }

        return false
    }

    private fun getFileListener(bolt : BoltBT,
                                msgID : Int) : BoltBytesListener {
        val infoPrefix = byteArrayOfInts(CMD_FILE_INFO, msgID)
        val donePrefix = byteArrayOfInts(CMD_FILE_DONE, 0, msgID)
        val bodyPrefix = byteArrayOfInts(CMD_FILE_BODY, msgID)
        val endPrefix = byteArrayOfInts(CMD_FILE_END, msgID)

        return BoltBytesListener(
            bolt,
            FILE_UUID,
            listOf(infoPrefix, donePrefix, bodyPrefix, endPrefix)
        )
    }

    private fun notifyProgress(bytesReceived : Int, fileSize : Int) {
        progressCallback(bytesReceived, fileSize)
    }
}

