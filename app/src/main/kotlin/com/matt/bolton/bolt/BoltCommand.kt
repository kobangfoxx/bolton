
package com.matt.bolton.bolt

public abstract class BoltCommand {
    public open class BoltCommandResult(val success : Boolean) { }

    /**
     * Execute the command on the given Bolt connection
     */
    abstract suspend fun send(bolt : BoltBT) : BoltCommandResult
}

