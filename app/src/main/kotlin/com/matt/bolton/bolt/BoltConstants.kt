
package com.matt.bolton.bolt

import java.nio.ByteOrder
import java.util.UUID
import java.util.regex.Pattern

import android.annotation.SuppressLint

val BOLT_ENCODING = Charsets.UTF_8
val BOLT_ENDIAN = ByteOrder.LITTLE_ENDIAN
val BOLT_SEP = "/"

val CONFIG_UUID
    = UUID.fromString("a026e019-0a7d-4ab3-97fa-f1500f9feb8b")
val FILE_UUID
    = UUID.fromString("a026e036-0a7d-4ab3-97fa-f1500f9feb8b")
val KEEP_ALIVE_UUID
    = UUID.fromString("a026e01c-0a7d-4ab3-97fa-f1500f9feb8b")
val CLIENT_CHARACTERISTIC_CONFIG
    = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

val BOLT_SERVICE_UUID
    = UUID.fromString("a026ee06-0a7d-4ab3-97fa-f1500f9feb8b")

val KEEP_ALIVE_MSG = byteArrayOfInts(0)

val CMD_FILE_LIST_BODY = 0x01
val CMD_FILE_LIST_END = 0x02
val CMD_FILE_LIST_RES_BODY = 0x04
val CMD_FILE_LIST_RES_END = 0x05
val CMD_FILE_NAME_BODY = 0x06
val CMD_FILE_NAME_END = 0x07
val CMD_FILE_INFO = 0x08
val CMD_FILE_DONE = 0x09
val CMD_FILE_BODY = 0x0a
val CMD_FILE_END = 0x0b
val CMD_FILE_RESPONSE = 0x0c
val CMD_START_ROUTE_BODY = 0x0d
val CMD_START_ROUTE_END = 0x0e
val CMD_START_ROUTE_RES = 0x0f
val CMD_LOAD_PLAN_BODY = 0x11
val CMD_LOAD_PLAN_END = 0x12
val CMD_LOAD_PLAN_RES = 0x13

val FILE_ACTION_GET = 0x03
val FILE_ACTION_DEL = 0x04

val FILE_LIST_FORMAT_DIR = 0.toByte() // name,time,num files
val FILE_LIST_FORMAT_FILE = 1.toByte() // name,time,size
val FILE_LIST_FORMAT_CHECK = 3.toByte() // name,time.size,checksum

val FILE_CHUNK_SIZE = 4096
val BOLT_ALLOWED_MSG_BYTES = 17

@SuppressLint("SdCardPath")
public enum class FileListingType(val baseDirectory : String) {
    USB("/sdcard/"),
    INTERNAL_ROUTES("/data/user/0/com.wahoofitness.bolt/files/routes/"),
    INTERNAL_PLANS("/data/user/0/com.wahoofitness.bolt/files/plans/")
}

// alternatives are 1 for just files, 2 for just dirs, 3 for files/dirs
// and checksums, 5 or later for gzipped versions
val FILE_LISTING_ALL_CHECK = 3

val PLAYABLE_PLANS_DIR_CODE : Map<String, Byte> = mapOf(
    FileListingType.INTERNAL_PLANS.baseDirectory + "0/" to 0.toByte(),
    FileListingType.INTERNAL_PLANS.baseDirectory + "1/" to 1.toByte(),
    FileListingType.INTERNAL_PLANS.baseDirectory + "2/" to 2.toByte(),
    FileListingType.INTERNAL_PLANS.baseDirectory + "3/" to 3.toByte(),
    FileListingType.INTERNAL_PLANS.baseDirectory + "4/" to 4.toByte(),
    FileListingType.INTERNAL_PLANS.baseDirectory + "5/" to 5.toByte(),
    FileListingType.INTERNAL_PLANS.baseDirectory + "6/" to 6.toByte(),
    FileListingType.INTERNAL_PLANS.baseDirectory + "7/" to 7.toByte()
)

val PLAN_FILE_EXT = ".plan"

// map from Bolt dirname to code for starting activity
val PLAYABLE_ROUTES_DIR_CODE : Map<String, Short> = mapOf(
    FileListingType.INTERNAL_ROUTES.baseDirectory + "EXT_FOLDER/"
        to 0x03ec.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "BBS-COURSE/"
        to 0x03eb.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "BBS-RACE/"
        to 0x03ea.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "KOMOOT/"
        to 0x0012.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "MTBPROJECT/"
        to 0x0021.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "RIDEWITHGPS/"
        to 0x000b.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "SINGLETRACKS/"
        to 0x0020.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "STRAVA/"
        to 0x0008.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "WAHOO/"
        to 0x000a.toShort(),
    FileListingType.INTERNAL_ROUTES.baseDirectory + "ASSET/"
        to 0x03e9.toShort()
)

val START_ROUTE_FORWARD = 0x01
val START_ROUTE_BACKWARD = 0x02

